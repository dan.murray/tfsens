import sys

dpi = None 
game = None
sens = None

def usage():
    print( "usage: tfsens.py [OPTION]...\n" )
    print( "compute either sensitivity in cm/360, in game sensitivity or dpi - assumes m_yaw 0.022" )
    print( "  -h,\tprint this help" )
    print( "  -d,\tyour dpi" )
    print( "  -g,\tyour in game sensitivity" )
    print( "  -s,\tyour sensitivity in cm/360" )
    print( "examples:" )
    print( "  python tf2sens.py -d 800 -g 3.3" )
    print( "  output sensitivity in cm/360 for a dpi of 800 and an in game sensitivity of 3.3\n" )
    print( "  python tf2sens.py -d 800 -s 15.744" )
    print( "  output in game sensitivity for a sensitivity of 15.744 cm/360 and a dpi of 800\n" )
    print( "created by smiley 2019" )

def compute_sens( dpi, game ):
    return ( 360 * 2.54 ) / ( 0.022 * dpi * game )

def compute_game( dpi, sens ):
    return ( 360 * 2.54 ) / ( 0.022 * dpi * sens )

def compute_dpi( game, sens ):
    return ( 360 * 2.54 ) / ( 0.022 * game * sens )

for index in xrange( 1, len( sys.argv ) ):
    arg = sys.argv[ index ]
    if arg[ 0 ] == '-':
        if arg[ 1 ] == 'h':
            usage()
            sys.exit( 0 )
        elif arg[ 1 ] == 'd':
            index = index + 1
            arg = sys.argv[ index ]
            dpi = float( arg )
        elif arg[ 1 ] == 'g':
            index = index + 1
            arg = sys.argv[ index ]
            game = float( arg )
        elif arg[ 1 ] == 's':
            index = index + 1
            arg = sys.argv[ index ]
            sens = float( arg )

if dpi != None and game != None and sens != None:
    print( "error: only specify two of dpi, in game sens and sens cm/360!" )
elif dpi != None and game != None:
    print( compute_sens( dpi, game ) )
elif dpi != None and sens != None:
    print( compute_game( dpi, sens ) )
elif game != None and sens != None:
    print( compute_dpi( game, sens ) )
else:
    usage()

